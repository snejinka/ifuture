(function ($) {
    $(function () {

        $('.testimonial__slider').slick({
            arrows:false,
            dots: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            adaptiveHeight: true,
            infinite:true,
        });

        $('.anchor').on('click', function(e){
            e.preventDefault();
            var elem = $(this).attr('href'),
                positionscroll = $(elem).offset().top;
            $('body,html').animate({scrollTop:positionscroll}, 1000);

        });

        $('.course-category__item').on({
            mouseenter:function (e) {
                $(this).addClass('active');
                var jumbotron = $(this).attr('href');
                $('.jumbotron.cource__jumbotron').hide();
                $(jumbotron).stop(true, true).fadeIn(700);
            },
            mouseleave:function () {
                $(this).removeClass('active');
            }
        });



    });
})(jQuery);